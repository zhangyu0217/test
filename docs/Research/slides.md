---
title : Reference
---
# Book

## Python

[Python:from introduction to programming](https://gitlab.com/zhangyu0217/homepage/-/blob/main/docs/Research/Python.pdf)
## Machine Learning

[The method of statistical learning, Hang LI](https://gitlab.com/zhangyu0217/homepage/-/blob/main/docs/Research/%E7%BB%9F%E8%AE%A1%E5%AD%A6%E4%B9%A0%E6%96%B9%E6%B3%95-%E6%9D%8E%E8%88%AA.pdf)

[The multi-variate statistical analysis of experimental data, Yongsheng ZHU](https://gitlab.com/zhangyu0217/homepage/-/blob/main/docs/Research/%E5%AE%9E%E9%AA%8C%E6%95%B0%E6%8D%AE%E5%A4%9A%E5%85%83%E7%BB%9F%E8%AE%A1%E5%88%86%E6%9E%90.pdf)

[Machine learning, Zhihua ZHOU](https://gitlab.com/zhangyu0217/homepage/-/blob/main/docs/Research/%E5%91%A8%E5%BF%97%E5%8D%8E-%E6%9C%BA%E5%99%A8%E5%AD%A6%E4%B9%A0.pdf)

[Deep learning, Goodfellow](https://gitlab.com/zhangyu0217/homepage/-/blob/main/docs/Research/%E6%B7%B1%E5%BA%A6%E5%AD%A6%E4%B9%A0-%E6%97%A0%E6%B0%B4%E5%8D%B0-%E4%B8%AD%E6%96%87%E7%89%88.pdf)

[GANs in actions](https://gitlab.com/zhangyu0217/homepage/-/blob/main/docs/Research/GAN%EF%BC%9A%E5%AE%9E%E6%88%98%E7%94%9F%E6%88%90%E5%AF%B9%E6%8A%97%E7%BD%91%E7%BB%9C.pdf)

[GANs in actions](https://gitlab.com/zhangyu0217/homepage/-/blob/main/docs/Research/GANs%20in%20Action%20Deep%20learning%20with%20Generative%20Adversarial%20Networks%209781617295560%20c.pdf)

## Quantum Computing

[Quantum Computing and Quantum Information, Nielsen, Chuang](https://gitlab.com/zhangyu0217/homepage/-/blob/main/docs/Research/QuantumComputing.pdf)

## Training Manual
[Training Manual](https://gitlab.com/zhangyu0217/homepage/-/blob/main/docs/Research/%E5%9F%B9%E8%AE%AD%E6%89%8B%E5%86%8C.pdf)
