---
title : Learning on quantum computing (1)
date : 2022-02-15
categories : quantum computing
tags : quantum computing
mathjax : true
---

Now I am starting my learning on quantum computing. Now I start with the textbook ["Mastering quantum computing with IMB QX"](https://github.com/PacktPublishing/Mastering-Quantum-Computing-with-IBM-QX).

# Quantum bit

## Three different experssions of quantum bit.

$$
\begin{cases}
|0>=\begin{bmatrix}
1\\
0
\end{bmatrix}
\\
|1>=\begin{bmatrix}
0\\
1
\end{bmatrix}
\end{cases}
$$

$$
\begin{cases}
|+>=\frac{1}{\sqrt{2}}(|0>+|1>)=\begin{bmatrix}
\frac{1}{\sqrt{2}} \\
\frac{1}{\sqrt{2}}
\end{bmatrix}
\\
|->=\frac{1}{\sqrt{2}}(|0>-|1>)=\begin{bmatrix}
\frac{1}{\sqrt{2}}\\
-\frac{1}{\sqrt{2}}
\end{bmatrix}
\end{cases}
$$

$$
\begin{cases}
|right>=\frac{1}{\sqrt{2}}(|0>+i|1>)=\frac{1}{\sqrt{2}}\begin{bmatrix}
1\\
i\\
\end{bmatrix}
\\
|left>=\frac{1}{\sqrt{2}}(|0>-i|1>)=\frac{1}{\sqrt{2}}\begin{bmatrix}
1\\
-i
\end{bmatrix}
\end{cases}
$$

|0> and |1> is the eigenvector of $\sigma\_{z}$. |+> and |-> is the eigenvector of $\sigma\_{x}$. |right> and |left> is the eigenvector of $\sigma\_{y}$.

$$
\begin{cases}
\sigma\_{x}=\begin{bmatrix}
0&1\\
1&0\\
\end{bmatrix}
\\
\sigma\_{y}=\begin{bmatrix}
0&-i\\
i&0\\
\end{bmatrix}
\\
\sigma\_{z}=\begin{bmatrix}
1&0\\
0&-1\\
\end{bmatrix}
\end{cases}
$$

## Bloch Sphere

A quantum state is corresponding to a vector on bloch sphere.
$$
\begin{cases}
|\Psi>=cos\frac{\theta}{2}|0>+e^{i\phi}sin\frac{\theta}{2}|1>\\
|\Psi>=\begin{bmatrix}
cos\frac{\theta}{2}&sin\theta e^{-i\phi}\\
sin\theta e^{i\phi}&-cos\frac{\theta}{2} \\
\end{bmatrix}|0>
\end{cases}
$$

$\begin{bmatrix}
cos\frac{\theta}{2}&sin\theta e^{-i\phi}\\
sin\theta e^{i\phi}&-cos\frac{\theta}{2} \\
\end{bmatrix}$ is the transformation of arbitrary rotation of $\theta$ and $\phi$.

# Quantum state, quantum register and measurement

## quantum state and quantum register

A quantum system can be expressed by the direct product of quantum states

## separable state

## quantum entanglement

If $|\psi>$ is defined as following,

$$|\psi>=\frac{1}{\sqrt{2}}(|00>+|11|)$$

it means qubit 1 and qubit 2 are totaly in entanglement.

## De-coherence, $T\_{1}$ and $T\_{2}$

# Quantum Gate

## Single Qubit Gate

### Hadamard Gate

$$
H=\begin{bmatrix}
1&1\\
1&-1\\
\end{bmatrix}
$$

This gate is corresponding to rotate 180 degree along x-axis and then rotate 90 degree along y-axis. 

$$
\begin{cases}
H|0>=|+>=\frac{1}{\sqrt{2}}(|0>+|1>)\\
H|1>=|->=\frac{1}{\sqrt{2}}(|0>-|1>)\\
H|+>=|0>\\
H|->=|1>\\
\end{cases}
$$

It is easy to see $HH=I$.

### Pauli Gate

+ X gate : $\sigma\_{x}=\begin{bmatrix}0&1\\1&0\end{bmatrix}$
+ Y gate : $\sigma\_{y}=\begin{bmatrix}0&-i\\i&0\end{bmatrix}$
+ Z gate : $\sigma\_{z}=\begin{bmatrix}1&0\\0&-1\end{bmatrix}$

### Phase Gate(S) and $\pi/8$ Gate(T)

$S=\begin{bmatrix}1&0\\0&e^{i\frac{1}{\pi}}\end{bmatrix}=\begin{bmatrix}1&0\\0&i\end{bmatrix}$

$$
\begin{cases}
S|0>=|0>\\
S|1>=|1>\\
S|+>=|right>\\
S|->=|left>\\
S|right>=|->\\
S|left>=|+>\\
SSSS|+>=|+>\\
\end{cases}
$$ 

It is easy to see SSSS=I.

S is the 90 degree rotation in x-y plane.


$T=\begin{bmatrix}1&0\\0&e^{i\frac{\pi}{4}}\end{bmatrix}=\begin{bmatrix}1&0\\0&\frac{1}{\sqrt{2}}(1+i)\end{bmatrix}$

T is the 45 degree rotation in x-y plane

S=TT

Dagger gate $\dagger{S}$ and $\dagger{T}$ is the Hermitian conjugate matrix of S and T.

## Multiple qubit gate

### Controlled-not Gate(CNOT)

If the first bit is |1>, then execute "NOT" on the second bit.

The input and output should be as following.

|Initial state | output of CNOT gate|
|---|---|
|00|00|
|01|01|
|10|11|
|11|10|


Define

$$
\begin{cases}
|00>=
\begin{bmatrix}
1 \\
0
\end{bmatrix}
\otimes
\begin{bmatrix}
1 \\
0
\end{bmatrix}
=
\begin{bmatrix}
1 \\
0 \\
0 \\
0
\end{bmatrix}
\\
|01>=\begin{bmatrix}
1 \\
0
\end{bmatrix}\otimes\begin{bmatrix}
0 \\
1
\end{bmatrix}
=\begin{bmatrix}
0 \\
1 \\
0 \\
0
\end{bmatrix}
\\
|10>=\begin{bmatrix}
0\\
1
\end{bmatrix}\otimes\begin{bmatrix}
1\\
0
\end{bmatrix}
=\begin{bmatrix}
0\\
0\\
1\\
0
\end{bmatrix}
\\
|11>=\begin{bmatrix}
0\\
1
\end{bmatrix}\otimes\begin{bmatrix}
0\\
1
\end{bmatrix}
=\begin{bmatrix}
0\\
0\\
0\\
1
\end{bmatrix}
\end{cases}
$$

Then CNOT=$\begin{bmatrix}1&0&0&0\\0&1&0&0\\0&0&0&1\\0&0&1&0\end{bmatrix}$ so that

$$
\begin{cases}
CNOT|00>=|00> \\
CNOT|01>=|01> \\
CNOT|10>=|11> \\
CNOT|11>=|10> \\
\end{cases}
$$

Let us consider 
$$
|starting>=|+0>=\frac{1}{\sqrt{2}}(|00>+|10>)
$$
Apply CNOT on the starting state, The output is 
$$
|final>=CNOT|starting>=\frac{1}{\sqrt{2}}(|00>+|11>)
$$
This means we generate an extangled state with CNOT gate.	
