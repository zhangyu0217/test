---
title : Slides
---


[Chapter1](https://gitlab.com/zhangyu0217/homepage/-/raw/main/docs/ThermodynamicsAndStatisticPhysics/%E7%AC%AC%E4%B8%80%E7%AB%A0_%E7%83%AD%E5%8A%9B%E5%AD%A6%E7%9A%84%E5%9F%BA%E6%9C%AC%E8%A7%84%E5%BE%8B.ppt)

[Chapter2](https://gitlab.com/zhangyu0217/homepage/-/raw/main/docs/ThermodynamicsAndStatisticPhysics/%E7%AC%AC%E4%BA%8C%E7%AB%A0_%E5%9D%87%E5%8C%80%E7%89%A9%E8%B4%A8%E7%9A%84%E7%83%AD%E5%8A%9B%E5%AD%A6%E6%80%A7%E8%B4%A8.ppt)

[Chapter3](https://gitlab.com/zhangyu0217/homepage/-/raw/main/docs/ThermodynamicsAndStatisticPhysics/%E7%AC%AC%E4%B8%89%E7%AB%A0_%E5%8D%95%E5%85%83%E7%B3%BB%E7%9A%84%E7%9B%B8%E5%8F%98.ppt)

[Jacobi](https://gitlab.com/zhangyu0217/homepage/-/raw/main/docs/ThermodynamicsAndStatisticPhysics/jacobi.pdf)

[textbook WANG Zhicheng](https://gitlab.com/zhangyu0217/homepage/-/raw/main/docs/ThermodynamicsAndStatisticPhysics/textbook_WANGZhicheng.pdf)

[Homework2](https://gitlab.com/zhangyu0217/homepage/-/raw/main/docs/ThermodynamicsAndStatisticPhysics/homework2.pdf)

[Review](https://gitlab.com/zhangyu0217/homepage/-/raw/main/docs/ThermodynamicsAndStatisticPhysics/lecture.pdf)
